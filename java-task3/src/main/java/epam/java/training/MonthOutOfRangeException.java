package epam.java.training;

public class MonthOutOfRangeException extends Exception{
    MonthOutOfRangeException(String message){
        super(message);
    }
}
