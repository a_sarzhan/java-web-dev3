package epam.java.training;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * Epam - Java Web Development Training
 * Assel Sarzhanova
 * Task 3
 */

public class App {
    public static void main( String[] args ) throws IOException {

        try(BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in))){

        int userEnteredYear;
        int userEnteredMonth;
        while(true) {
            try {
                System.out.println("Enter Year :");
                userEnteredYear = Integer.parseInt(bufferedReader.readLine());
                validateUserInput(userEnteredYear);
                break;
            } catch (NegativeUserInputException e) {
                e.printStackTrace();
            }
        }

        while(true){
            try{
                System.out.println("Enter month in number format (from 1 to 12) :");
                userEnteredMonth = Integer.parseInt(bufferedReader.readLine());
                validateUserInput(userEnteredMonth);
                validateMonthRange(userEnteredMonth);
                break;
            }catch(NegativeUserInputException e){
                e.printStackTrace();
            }catch(MonthOutOfRangeException e){
                e.printStackTrace();
            }
        }

        String yearType = isLeapYear(userEnteredYear) ? "leap year" : "not a leap year";
        System.out.println("----------------------------------------------------------");
        System.out.printf("%d %s - %s\n",userEnteredYear, Month.values()[userEnteredMonth-1], yearType);
        System.out.println("----------------------------------------------------------");

        }
    }

    public static void validateUserInput(int input) throws NegativeUserInputException{
        if(input < 0)
            throw new NegativeUserInputException("Negative Input Value = " + input);
    }

    public static void validateMonthRange(int monthNumber) throws MonthOutOfRangeException{
        if(monthNumber < 1 || monthNumber > 12)
            throw new MonthOutOfRangeException("Month out of range. Month number = " + monthNumber);
    }

    public static boolean isLeapYear(int year){
        return ((year % 400 == 0) || (year % 4 == 0 && year % 100 > 0));
    }
}
