package epam.java.training;

public class NegativeUserInputException extends Exception {
    NegativeUserInputException(String message){
        super(message);
    }
}
